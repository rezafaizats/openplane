#pragma once
class GLProgram
{
	public :
		void LoadProgram();

	private :
};

struct Vertex
{

	glm::vec3 position;
	glm::vec3 color;
	glm::vec2 texcoord;

};