// OpenPlane.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "LibraryList.h"

void framebuffer_resize_callback(GLFWwindow* window, int fbw, int fbh)
{
	glViewport(0, 0, fbw, fbh);
}

static const GLfloat g_vertex_buffer_data[] = {
	-1.0f, -1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	0.f, 1.0f, 0.0f
};

GLuint LoadShaders(const char* vertex_path, const char* fragment_path) {

	//Create Shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	ifstream in_file;
	string src = " ";
	string temp = " ";

	//Read vertex shader file
	in_file.open("VertexCore.glsl");
	if (in_file.is_open())
	{
		while (std::getline(in_file, temp))
		{
			src += temp + "\n";
		}
	}
	else
	{
		std::cout << "Error load shader couldn't open vertex file \n";
	}
	in_file.close();

	//Compile Vertex Shader
	cout << "Compiling vertex shader : " << src << endl;
	const GLchar* vertSrc = src.c_str();
	glShaderSource(VertexShaderID, 1, &vertSrc, NULL);
	glCompileShader(VertexShaderID);

	//Check Vertex Shader
	GLint result = GL_FALSE;
	int InfoLogLength;
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);

	if (InfoLogLength > 0)
	{
		vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		cout << "Error compiling vertex shader : " << &VertexShaderErrorMessage[0] << endl;
	}

	//Read Fragment Shader
	src = " ";
	temp = " ";

	in_file.open("FragmentCore.glsl");
	if (in_file.is_open())
	{
		while (std::getline(in_file, temp))
		{
			src += temp + "\n";
		}
	}
	else
	{
		std::cout << "Error load shader couldn't open vertex file \n";
	}
	in_file.close();

	//Compile Fragment Shader
	cout << "Compiling fragment shader : " << src << endl;
	const GLchar* fragSrc = src.c_str();
	glShaderSource(FragmentShaderID, 1, &fragSrc, NULL);
	glCompileShader(FragmentShaderID);

	//Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);

	if (InfoLogLength > 0)
	{
		vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		cout << "Error compiling vertex shader : " << &FragmentShaderErrorMessage[0] << endl;
	}

	//Program Linking
	cout << "Linking to program..." << endl;
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	//Check Program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);

	if (InfoLogLength > 0)
	{
		vector<char> ProgramErrorMessage(InfoLogLength + 1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		cout << &ProgramErrorMessage[0] << endl;
	}

	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}

void CenterString(string text, bool isInput) {

	int console_width = 80;
	int len = text.length();

	if (len % 2 == 0)
		text += " ";

	if (!isInput)
		cout << setw((int)(console_width / 2) + (len / 2)) << right << text << endl;
	else
		cout << setw((int)(console_width / 2) + (len / 2)) << right << text;

}
void menu() {

	CenterString("Welcome to Open Plane", false);
	CenterString("1. Play", false);
	CenterString("2. About", false);
	CenterString("3. Quit", false);

}

void about() {

	CenterString("Created by : Reza Faiz Abdillah T. S.", false);
	CenterString("1. Back", false);

}

int main()
{
	bool isQuit, isMenu, isGame, isAbout;

	isQuit = false;
	isMenu = true;
	isGame = false;
	isAbout = false;

	glfwInit();

	//GLFW Window
	glfwWindowHint(GLFW_SAMPLES, 4);//Anti aliasing 4X
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);//OpenGL 4.0
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window;
	int frameBufferHeight = 0;
	int frameBufferWidth = 0;

	window = glfwCreateWindow(1024, 768, "Open Plane", NULL, NULL);
	glfwGetFramebufferSize(window, &frameBufferWidth, &frameBufferHeight);
	glfwSetFramebufferSizeCallback(window, framebuffer_resize_callback);
	glfwMakeContextCurrent(window);

	//GLEW
	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		std::cout << "ERROR::MAIN.CPP::GLEW_INIT_FAILED" << "\n";
		glfwTerminate();
	}

	//OpenGL Options
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//VAO
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	//Draw Buffer
	GLuint vertex_buffer;

	glGenBuffers(1, &vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

	//Model Matrix
	glm::vec3 position(0.f);
	glm::vec3 rotation(0.f);
	glm::vec3 scale(1.f);

	glm::mat4 ModelMatrix(1.f);
	ModelMatrix = glm::translate(ModelMatrix, position);
	ModelMatrix = glm::rotate(ModelMatrix, rotation.x, glm::vec3(1.f, 0.f, 0.f));
	ModelMatrix = glm::rotate(ModelMatrix, rotation.y, glm::vec3(0.f, 1.f, 0.f));
	ModelMatrix = glm::rotate(ModelMatrix, rotation.z, glm::vec3(0.f, 0.f, 1.f));
	ModelMatrix = glm::scale(ModelMatrix, scale);

	//Init Camera
	glm::vec3 camPos(0.f, 0.f, 1.f);
	glm::vec3 worldUp(0.f, 1.f, 0.f);
	glm::vec3 camFront(0.f, 0.f, -1.f);
	glm::mat4 ViewMatrix(1.f);
	ViewMatrix = glm::lookAt(camPos, camPos + camFront, worldUp);

	float fov = 90.f;
	float nearPlane = 0.1f;
	float farPlane = 1000.f;
	glm::mat4 ProjectionMatrix(1.f);

	ProjectionMatrix = glm::perspective(
		glm::radians(fov),
		static_cast<float>(frameBufferWidth) / frameBufferHeight,
		nearPlane,
		farPlane
	);

	//Init Uniforms
	GLuint programID = LoadShaders("VertexCore.glsl", "FragmentCore.glsl");
	//glUseProgram(programID);

	//glUniformMatrix4fv(glGetUniformLocation(programID, "ModelMatrix"), 1, GL_FALSE, glm::value_ptr(ModelMatrix));
	//glUniformMatrix4fv(glGetUniformLocation(programID, "ViewMatrix"), 1, GL_FALSE, glm::value_ptr(ViewMatrix));
	//glUniformMatrix4fv(glGetUniformLocation(programID, "ProjectionMatrix"), 1, GL_FALSE, glm::value_ptr(ProjectionMatrix));

	//glUseProgram(0);

	//Get Input
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	//Main Loop Program
	do
	{
		//Clear
		glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Draw
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);

		glVertexAttribPointer(
			0,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			(void*)0
		);

		glDrawArrays(GL_TRIANGLES, 0, 3);
		glDisableVertexAttribArray(0);

		//Program
		glUseProgram(programID);

		//Swap Buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0);

	/*while (!isQuit)
	{
		while (isMenu && !isGame && !isAbout)
		{
			int choose_menu = 0;

			menu();

			CenterString("Input menu : ", true);
			cin >> choose_menu;

			switch (choose_menu)
			{
			case 1:
				isMenu = false;
				isGame = true;
				isAbout = false;
				isQuit = false;
				break;
			case 2:
				isMenu = false;
				isGame = false;
				isAbout = true;
				isQuit = false;
				break;
			case 3:
				isMenu = false;
				isGame = false;
				isAbout = false;
				isQuit = true;
				break;
			default:
				CenterString("Input not identified!", false);
				break;
			}

			sleep_for(250ms);

			system("CLS");
		}

		while (isAbout && !isGame && !isMenu)
		{
			int choose_menu = 0;

			about();

			CenterString("Input menu : ", true);
			cin >> choose_menu;

			switch (choose_menu)
			{
			case 1:
				isMenu = true;
				isGame = false;
				isAbout = false;
				break;
			default:
				CenterString("Input not identified!", false);
				break;
			}

			sleep_for(250ms);

			system("CLS");
		}

		if (isQuit)
			break;
	}*/

	cin.ignore();
	return 0;
}

